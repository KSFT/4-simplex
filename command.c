/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "defs.h"
#include "utils.h"
#include "db.h"
#include "player.h"

#include "command.h"

void cmd_handle(struct player *p, char *cmd_)
{
    printf("%s uses '%s'\n", p->name, cmd_);
    char *cmd = strtok(cmd_ + 1, " ");
    if(!cmd)
        return;

    if(strcmp(cmd, "help") == 0)
    {
        if(strtok(NULL, " ") != NULL)
        {
            player_msg(p, "&eusage: /help");
            return;
        }
        player_msgf(p, "&eThis server runs %s. Only a few commands are availabl\
e so far: /help, /a, /block, /group, /kick, /roll, /tp, /s, /save, and /z.\nhtt\
ps://gitlab.com/KSFT/4-simplex", NAME);
        return;
    }

    else if(strcmp(cmd, "a") == 0)
    {
        player_block_switch(p);
        p->state_block = STATE_BLOCK_NORMAL;
    }
    else if(strcmp(cmd, "block") == 0)
    {
        char *arg1 = strtok(NULL, " ");
        if(strtok(NULL, " "))
        {
            player_msg(p, "&eusage: /block, /block <block ID>");
            return;
        }

        if(arg1)
        {
            if(p->state_block != STATE_BLOCK_BLOCK)
                player_block_switch(p);

            p->state_block = STATE_BLOCK_BLOCK;
            int id = estrtoi(arg1);
            if(id == -1 || id == 0)
            {
                player_msg(p, "&eblock ID must be a positive number");
                return;
            }
            p->block_id = id;
            player_msgf(p, "&enow placing block ID %d", id);
            return;
        }
        if(p->block_id == 0)
        {
            player_msg(p, "&eno block ID specified");
            return;
        }

        player_block_switch(p);
        if(p->state_block == STATE_BLOCK_BLOCK)
            p->state_block = STATE_BLOCK_NORMAL;
        else
        {
            p->state_block = STATE_BLOCK_BLOCK;
            player_msgf(p, "&eresuming placing block ID %d", p->block_id);
        }
    }
    else if(strcmp(cmd, "group") == 0)
    {
        char *arg1 = strtok(NULL, " ");
        if(!arg1)
        {
            player_msg(p, "&eusages:");
            player_msg(p, "&e/group info <group>");
            player_msg(p, "&e/group list");
            player_msg(p, "&e/group list <player>");
            player_msg(p, "&e/group players <group>");
            player_msg(p, "&e/group set <player> <group>");
            player_msg(p, "&e/group unset <player> <group>");
            player_msg(p, "&e/group add <group> <permission>");
            player_msg(p, "&e/group remove <group> <permission>");
            player_msg(p, "&e/group create <group>");
            player_msg(p, "&e/group delete <group>");
            return;
        }
        if(strcmp(arg1, "info") == 0)
        {
            char *arg2 = strtok(NULL, " ");
            if(!arg2 || strtok(NULL, " "))
            {
                player_msg(p, "&eusage: /group info <group>");
            }

            if(db_group_get_id(arg2) == -1)
            {
                player_msg(p, "&eno such group");
                return;
            }
            struct alist *permissions = db_group_get_permissions(arg2);

            char *list = alist_ejoin(permissions, ", ");
            player_msgf(p, "&epermissions for group %s:\n%s", arg2, list);
            free(list);

            for(size_t i = 0; i < permissions->size; i++)
                free(permissions->items[i]);
            free(permissions->items);
            free(permissions);
        }
        else if(strcmp(arg1, "list") == 0)
        {
            char *arg2 = strtok(NULL, " ");
            if(arg2)
            {
                char *arg3 = strtok(NULL, " ");
                if(strtok(NULL, " ") || (arg3 && strcmp(arg3, "+") != 0))
                {
                    player_msg(p, "&eusage: /group list <player>");
                    return;
                }

                char *name;
                if(db_player_get_id(arg2) == -1)
                {
                    if(arg3)
                    {
                        player_msg(p, "&eplayer not found");
                        return;
                    }
                    struct player *q = NULL;
                    if(player_find_one(p, &q, arg2) > 1)
                        return;
                    if(q)
                        name = q->name;
                    else
                    {
                        player_msg(p, "&eplayer not found");
                        return;
                    }
                }
                else
                    name = arg2;

                struct alist *groups = db_player_get_groups(name);

                char *list = alist_ejoin(groups, ", ");
                player_msgf(p, "&e%s is in these groups:\n%s", name, list);
                free(list);

                for(size_t i = 0; i < groups->size; i++)
                    free(groups->items[i]);
                free(groups->items);
                free(groups);
            }
            else
            {
                struct alist *groups = db_group_get_list();

                char *list = alist_ejoin(groups, ", ");
                player_msgf(p, "&egroups:\n%s", list);
                free(list);

                for(size_t i = 0; i < groups->size; i++)
                    free(groups->items[i]);
                free(groups->items);
                free(groups);
            }
        }
        else if(strcmp(arg1, "players") == 0)
        {
            char *arg2 = strtok(NULL, " ");
            if(!arg2 || strtok(NULL, " "))
            {
                player_msg(p, "&eusage: /group players <group>");
                return;
            }
            if(db_group_get_id(arg2) == -1)
            {
                player_msg(p, "&eno such group");
                return;
            }
            struct alist *players = db_group_get_players(arg2);

            char *list = alist_ejoin(players, ", ");
            player_msgf(p, "&e%s:\n%s", arg2, list);
            free(list);

            for(size_t i = 0; i < players->size; i++)
                free(players->items[i]);
            free(players->items);
            free(players);
        }
        else if(strcmp(arg1, "set") == 0)
        {
            char *arg2 = strtok(NULL, " ");
            char *arg3 = strtok(NULL, " ");
            if(!arg2 || !arg3 || strtok(NULL, " "))
            {
                player_msg(p, "&eusage: /group set <player> <group>");
                return;
            }

            struct player *q = NULL;
            if(player_find_one(p, &q, arg2) > 1)
                return;

            char *name;
            if(q)
                name = q->name;
            else
                name = arg2;

            if(db_group_get_id(arg3) == -1)
            {
                player_msg(p, "&eno such group");
                return;
            }

            char *perm = emalloc(11+strlen(arg3));
            strcpy(perm, "group-set-");
            strcpy(perm + 10, arg3);
            int has_perm = db_player_has_permission(p->name, perm)
                || db_player_has_permission(p->name, PERMISSION_GROUP_SETALL)
                || db_player_has_permission(p->name, PERMISSION_GROUP_ALL);
            free(perm);

            if(!has_perm)
            {
                player_msg(p, "&eyou are not allowed to set that group");
                return;
            }
            if(db_player_is_group(name, arg3))
            {
                player_msg(p, "&ethat player is already in that group");
                return;
            }

            db_player_add_group(name, arg3);

            if(q)
            {
                player_msgf(q, "&eyou are now in group %s", arg3);
                broadcast_msgf("&e%s is now in group %s", name, arg3);
            }
            else
                broadcast_msgf("&eoffline player %s is now in group %s",
                               name, arg3);
        }
        else if(strcmp(arg1, "unset") == 0)
        {
            char *arg2 = strtok(NULL, " ");
            char *arg3 = strtok(NULL, " ");
            if(!arg2 || !arg3 || strtok(NULL, " "))
            {
                player_msg(p, "&eusage: /group unset <player> <group>");
                return;
            }

            struct player *q = NULL;
            if(player_find_one(p, &q, arg2) > 1)
                return;

            char *name;
            if(q)
                name = q->name;
            else
                name = arg2;

            if(db_group_get_id(arg3) == -1)
            {
                player_msg(p, "&eno such group");
                return;
            }

            char *perm = emalloc(13+strlen(arg3));
            strcpy(perm, "group-unset-");
            strcpy(perm + 12, arg3);
            int has_perm = db_player_has_permission(p->name, perm)
                || db_player_has_permission(p->name, PERMISSION_GROUP_SETALL)
                || db_player_has_permission(p->name, PERMISSION_GROUP_ALL);
            free(perm);

            if(!has_perm)
            {
                player_msg(p, "&eyou are not allowed to unset that group");
                return;
            }
            if(!db_player_is_group(name, arg3))
            {
                player_msg(p, "&ethat player is not in that group");
                return;
            }

            db_player_remove_group(name, arg3);

            if(q)
            {
                player_msgf(q, "&eyou are no longer in group %s", arg3);
                broadcast_msgf("&e%s is no longer in group %s", name, arg3);
            }
            else
                broadcast_msgf("&eoffline player %s is no longer in group %s",
                               name, arg3);
        }
        else if(strcmp(arg1, "add") == 0)
        {
            char *arg2 = strtok(NULL, " ");
            char *arg3 = strtok(NULL, " ");
            if(!arg2 || !arg3 || strtok(NULL, " "))
            {
                player_msg(p, "&eusage: /group add <group> <permission>");
                return;
            }

            if(!((db_player_has_permission(p->name, arg3)
                  && db_player_has_permission(p->name, PERMISSION_GROUP_ADD))
                 || db_player_has_permission(p->name, PERMISSION_GROUP_ALL)))
            {
                player_msg(p, "&eyou are not allowed to add that permission");
                return;
            }

            if(db_group_get_id(arg2) == -1)
            {
                player_msg(p, "&eno such group");
                return;
            }

            if(db_group_has_permission(arg2, arg3))
            {
                player_msg(p, "&ethat group already has that permission");
                return;
            }

            db_group_add_permission(arg2, arg3);

            broadcast_msgf("&egroup %s now has permission %s", arg2, arg3);
        }
        else if(strcmp(arg1, "remove") == 0)
        {
            char *arg2 = strtok(NULL, " ");
            char *arg3 = strtok(NULL, " ");
            if(!arg2 || !arg3 || strtok(NULL, " "))
            {
                player_msg(p, "&eusage: /group remove <group> <permission>");
                return;
            }

            if(!((db_player_has_permission(p->name, arg3)
                  && db_player_has_permission(p->name, PERMISSION_GROUP_REMOVE))
                 || db_player_has_permission(p->name, PERMISSION_GROUP_ALL)))
            {
                player_msg(p,
                           "&eyou are not allowed to remove that permission");
                return;
            }

            if(db_group_get_id(arg2) == -1)
            {
                player_msg(p, "&eno such group");
                return;
            }

            if(!db_group_has_permission(arg2, arg3))
            {
                player_msg(p, "&ethat group does not have that permission");
                return;
            }

            db_group_remove_permission(arg2, arg3);
            broadcast_msgf("&egroup %s no longer has permission %s",
                           arg2, arg3);
        }
        else if(strcmp(arg1, "create") == 0)
        {
            char *arg2 = strtok(NULL, " ");
            if(!arg2 || strtok(NULL, " "))
            {
                player_msg(p, "&eusage: /group create <group>");
                return;
            }

            if(!(db_player_has_permission(p->name, PERMISSION_GROUP_CREATE)
                 || db_player_has_permission(p->name, PERMISSION_GROUP_ALL)))
            {
                player_msg(p, "&eyou are not allowed to create groups");
                return;
            }

            db_group_create(arg2);
            broadcast_msgf("&egroup %s was created", arg2);
        }
        else if(strcmp(arg1, "delete") == 0)
        {
            char *arg2 = strtok(NULL, " ");
            if(!arg2 || strtok(NULL, " "))
            {
                player_msg(p, "&eusage: /group delete <group>");
                return;
            }

            if(!(db_player_has_permission(p->name, PERMISSION_GROUP_DELETE)
                 || db_player_has_permission(p->name, PERMISSION_GROUP_ALL)))
            {
                player_msg(p, "&eyou are not allowed to delete groups");
                return;
            }

            db_group_delete(arg2);
            broadcast_msgf("&egroup %s was deleted", arg2);
        }
    }
    else if(strcmp(cmd, "kick") == 0)
    {
        if(!db_player_has_permission(p->name, PERMISSION_KICK))
        {
            player_msg(p, "&eyou are not allowed to use /kick");
            return;
        }

        char *arg1 = strtok(NULL, " ");
        if(arg1 == NULL || strtok(NULL, " ") != NULL)
        {
            player_msg(p, "&eusage: /kick <player>");
            return;
        }
        struct player *q = NULL;
        if(player_find_one(p, &q, arg1) > 1)
            return;
        if(!q)
        {
            player_msg(p, "&eplayer not found");
            return;
        }

        char reason[11+strlen(p->name)];
        sprintf(reason, "kicked by %s", p->name);
        player_disconnect(q, reason);
    }
    else if(strcmp(cmd, "roll") == 0)
    {
        if(!db_player_has_permission(p->name, PERMISSION_ROLL))
        {
            player_msg(p, "&eyou are not allowed to use /roll");
            return;
        }

        char *arg1 = strtok(NULL, " ");
        if(arg1)
        {
            player_msg(p, "&eusage: /roll");
            return;
        }

        broadcast_msgf("%s rolled a %d", p->name, (rand() % 7) + 1);
    }
    else if(strcmp(cmd, "s") == 0)
    {
        if(!db_player_has_permission(p->name, PERMISSION_SUMMON))
        {
            player_msg(p, "&eyou are not allowed to use /s");
            return;
        }

        char *arg1 = strtok(NULL, " ");
        if(!arg1 || strtok(NULL, " "))
        {
            player_msg(p, "&eusage: /s <player>");
            return;
        }
        struct player *q = NULL;
        if(player_find_one(p, &q, arg1) > 1)
            return;
        if(!q)
        {
            player_msg(p, "&eplayer not found");
            return;
        }
        player_tp(q, p->x, p->y, p->z, p->yaw, p->pitch);
    }
    else if(strcmp(cmd, "save") == 0)
    {
        if(strtok(NULL, " "))
        {
            player_msg(p, "&eusage: /save");
            return;
        }
        if(!db_player_has_permission(p->name, PERMISSION_SAVE))
        {
            player_msg(p, "&eyou are not allowed to use /save");
            return;
        }
        lvl_save(p->level);
        broadcast_msgf("&elevel saved by %s", p->name);
    }
    else if(strcmp(cmd, "tp") == 0)
    {
        if(!db_player_has_permission(p->name, PERMISSION_TP))
        {
            player_msg(p, "&eyou are not allowed to use /tp");
            return;
        }

        char *arg1 = strtok(NULL, " ");
        if(!arg1 || strtok(NULL, " "))
        {
            player_msg(p, "&eusage: /tp <player>");
            return;
        }
        struct player *q = NULL;
        if(player_find_one(p, &q, arg1) > 1)
            return;
        if(!q)
        {
            player_msg(p, "&eplayer not found");
            return;
        }
        player_tp(p, q->x, q->y, q->z, q->yaw, q->pitch);
    }
    else if(strcmp(cmd, "z") == 0)
    {
        if(!db_player_has_permission(p->name, PERMISSION_CUBOID))
        {
            player_msg(p, "&eyou are not allowed to use /z");
            return;
        }

        char *arg1 = strtok(NULL, " ");
        if(arg1 != NULL)
        {
            player_msg(p, "&eusage: /z");
            return;
        }

        player_block_switch(p);
        p->state_block = STATE_BLOCK_CUBOID;
        player_msg(p, "&ecuboiding...");
    }
    else
        player_msg(p, "&eno such command");
}

void cmd_cuboid_block(struct player *p,
                      short_type x, short_type y, short_type z,
                      byte_type block)
{
    if(p->cuboid_x == -1)
    {
        p->cuboid_x = x;
        p->cuboid_y = y;
        p->cuboid_z = z;
    }
    else
    {
        int x1 = min(x, p->cuboid_x);
        int x2 = max(x, p->cuboid_x);
        int y1 = min(y, p->cuboid_y);
        int y2 = max(y, p->cuboid_y);
        int z1 = min(z, p->cuboid_z);
        int z2 = max(z, p->cuboid_z);

        player_msgf(p, "&eperforming cuboid of %d blocks",
                    (1+x2-x1) * (1+y2-y1) * (1+z2-z1));
        for(int i = x1; i <= x2; i++)
            for(int j = y1; j <= y2; j++)
                for(int k = z1; k <= z2; k++)
                    lvl_set_block(p->level, i, j, k, block);
        p->cuboid_x = p->cuboid_y = p->cuboid_z = -1;
        p->state_block = STATE_BLOCK_NORMAL;
    }
}

void cmd_cuboid_cancel(struct player *p)
{
    p->cuboid_x = p->cuboid_y = p->cuboid_z = -1;
    player_msg(p, "&ecuboid canceled");
}

void cmd_block_cancel(struct player *p)
{
    player_msgf(p, "&eno longer placing block ID %d", p->block_id);
}
