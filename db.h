/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#ifndef DB_H
#define DB_H

#include "utils.h"

int db_init();
void db_group_add_permission(const char *group, const char *permission);
void db_group_create(const char *group);
void db_group_delete(const char *group);
int db_group_get_id(const char *group);
struct alist *db_group_get_list();
struct alist *db_group_get_permissions(const char *group);
struct alist *db_group_get_players(const char *group);
int db_group_has_permission(const char *group, const char *permission);
void db_group_remove_permission(const char *group, const char *permission);
void db_player_add_group(const char *player, const char *group);
void db_player_add_group_id(const char *player, int group_id);
int db_player_is_group(const char *player, const char *group);
void db_player_remove_group(const char *player, const char *group);
struct alist *db_player_get_groups(const char *player);
int db_player_get_id(const char *player);
int db_player_has_permission(const char *player, const char *permission);
void db_player_add(const char *player);

#endif
