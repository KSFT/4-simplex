# Copyright 2018
# This file is licensed under the GNU GPL version 3 or any later version. See
# the LICENSE file.

 echo .read init.sql | sqlite3 playerdata.db
sqlite3 playerdata.db "
INSERT INTO Player(name)
VALUES ('$1');

INSERT INTO PlayerGroup(player, pgroup)
SELECT Player.id, PGroup.id
FROM Player, PGroup
WHERE Player.name = '$1'
    AND PGroup.name = 'all';
"
