/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include <stdio.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <sys/select.h>
#include <openssl/md5.h>

#include "defs.h"
#include "utils.h"
#include "db.h"
#include "config.h"
#include "packets.h"
#include "player.h"
#include "command.h"
#include "level.h"
#include "heartbeat.h"

void handle_connect(int fd);
void handle_data(int fd);
void handshake(struct player *p);
void handle_pkt_id(struct player *p);
void handle_pkt_block(struct player *p);
void handle_pkt_pos(struct player *p);
void handle_pkt_msg(struct player *p);
void ping();

fd_set master_fds;
fd_set read_fds;
int max_fd;

int main()
{
    if(db_init() == -1)
    {
        fprintf(stderr, "error loading player database %s\n", FILE_PLAYERDATA);
        return 1;
    }

    if(config_load() == -1)
    {
        fprintf(stderr, "error loading config file %s\n", FILE_CONFIG);
        return 1;
    }

    if(access(conf.mainlvl, R_OK) == -1)
    {
        printf("no main level detected at %s, generating new one...\n",
               conf.mainlvl);
        if(lvl_gen_flat(&main_lvl, conf.mainlvl, 128, 128, 128) == -1)
        {
            fprintf(stderr, "error generating main level\n");
            return -1;
        }
    }
    else if(lvl_load(&main_lvl, conf.mainlvl) == -1)
    {
        fprintf(stderr, "error loading main level %s\n", conf.mainlvl);
        return 1;
    }

    llist_init(&players);
    player_init_ids();

    if(alist_init(&levels) == -1)
    {
        perror("malloc");
        return 1;
    }
    alist_eadd(&levels, &main_lvl);

    struct timeval timeout;
    timeout.tv_sec = 1;
    timeout.tv_usec = 0;
    time_t prev_heartbeat, prev_ping, prev_save,
        curr_time;

    struct sockaddr_in addr;

    memset(&addr, 0, sizeof addr);
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(conf.port);

    int fd, sock_id = socket(PF_INET, SOCK_STREAM, 0);

    max_fd = sock_id;

    FD_ZERO(&master_fds);
    FD_SET(sock_id, &master_fds);

    int on = 1;

    if(sock_id == -1 ||
       setsockopt(sock_id, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on) == -1 ||
       bind(sock_id, (struct sockaddr *) &addr, sizeof addr) == -1 ||
       listen(sock_id, 1) == -1)
    {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    // ignore writes to closed sockets -- they'll be dealt with on the next
    // select() call
    signal(SIGPIPE, SIG_IGN);

    printf("server started on port %d\n", conf.port);
    heartbeat();
    prev_save = prev_ping = prev_heartbeat = time(NULL);

    while(1)
    {
        read_fds = master_fds;
        if(select(max_fd + 1, &read_fds, NULL, NULL, &timeout) == -1)
        {
            perror("select");
            exit(EXIT_FAILURE);
        }
        for(int i = 0; i < max_fd + 1; i++)
        {
            if(FD_ISSET(i, &read_fds))
            {
                if(i == sock_id)
                {
                    if((fd = accept(sock_id, NULL, NULL)) == -1)
                        perror("accept");
                    else
                        handle_connect(fd);
                }
                else
                    handle_data(i);
            }
        }
        curr_time = time(NULL);
        if(curr_time - prev_heartbeat >= conf.heartbeatinterval)
        {
            heartbeat();
            prev_heartbeat = time(NULL);
        }
        if(curr_time - prev_ping >= 1)
        {
            ping();
            prev_ping = time(NULL);
        }
        if(curr_time - prev_save >= conf.saveinterval)
        {
            for(size_t i = 0; i < levels.size; i++)
            {
                struct level *l = levels.items[i];
                if(l->changed)
                    lvl_save(l);
            }
            broadcast_msg("&eall levels automatically saved");
            prev_save = time(NULL);
        }

        timeout.tv_sec = 1;
        timeout.tv_usec = 0;
    }
    return 0;
}

void handle_connect(int fd)
{
    struct player *new = malloc(sizeof(struct player));
    if(new == NULL)
    {
        perror("malloc");
        return;
    }

    llist_append(&players, new);

    FD_SET(fd, &master_fds);
    if(fd > max_fd)
        max_fd = fd;
    new->buf_size = 0;
    new->connected = 0;
    new->fd = fd;
    new->level = NULL;
    new->state_block = STATE_BLOCK_NORMAL;
    new->state_chat = STATE_CHAT_NORMAL;
    new->cuboid_x = new->cuboid_y = new->cuboid_z = -1;
    new->block_id = 0;
    new->pmsg = emalloc(1);
    new->pmsg[0] = '\0';
}

void handle_data(int fd)
{
    struct player *p = NULL;
    for(struct llist_item *i = players.first; i; i = i->next)
        if(((struct player *) i->val)->fd == fd)
            p = i->val;
    if(!p)
        return;

    byte_type p_type;
    if(p->buf_size > 0)
        p_type = p->buf[0];
    else
    {
        if(read(fd, &p_type, 1) == 0)
        {
            if(p->connected)
                player_disconnect(p, "quit");
            return;
        }
        p->buf[0] = p_type;
        p->buf_size = 1;
    }
    if(!p->connected && p_type != 0x00)
    {
        char reason[64];
        pad64(reason, "not logged in");
        byte_type id = 0x0e;
        write(p->fd, &id, 1);
        write(p->fd, reason, 64);
        close(p->fd);
        FD_CLR(p->fd, &master_fds);
        free(p);
        return;
    }
    if(p->connected && p_type == 0x00)
    {
        player_disconnect(p, "bad packet received");
        return;
    }

    int p_size = pkt_size(p_type);
    if(p_size == -1)
    {
        player_disconnect(p, "invalid packet id");
        return;
    }
    int bytes_read = read(fd, p->buf + p->buf_size, p_size - p->buf_size);
    if(bytes_read == 0)
    {
        player_disconnect(p, "quit");
        return;
    }
    if((p->buf_size += bytes_read) < p_size)
        return;
    p->buf_size = 0;

    switch(p_type)
    {
        case 0x00:
            handle_pkt_id(p);
            break;
        case 0x05:
            handle_pkt_block(p);
            break;
        case 0x08:
            handle_pkt_pos(p);
            break;
        case 0x0d:
            handle_pkt_msg(p);
            break;
        default:
            player_disconnect(p, "bad packet received");
            return;
    }
}

void handshake(struct player *p)
{
    byte_type id = 0x00;
    byte_type version = 0x07;
    byte_type player_type = 0x00;
    char name[64];
    pad64(name, conf.name);
    char motd[64];
    pad64(motd, conf.motd);
    write(p->fd, &id, 1);
    write(p->fd, &version, 1);
    write(p->fd, &name, 64);
    write(p->fd, &motd, 64);
    write(p->fd, &player_type, 1);
}

int check_name(char *name, char *key_)
{
    char key[65];
    strip64(key, key_);

    uint8_t digest[MD5_DIGEST_LENGTH];
    char hex[3];
    MD5_CTX ctx;
    MD5_Init(&ctx);
    MD5_Update(&ctx, "0", 1);
    MD5_Update(&ctx, name, strlen(name));
    MD5_Final(digest, &ctx);
    for(int i = 0; i < MD5_DIGEST_LENGTH; i++)
    {
        sprintf(hex, "%02x", digest[i]);
        if(hex[0] != (uint8_t) key[2*i] || hex[1] != key[2*i + 1])
            return 0;
    }
    return 1;
}

void handle_pkt_id(struct player *p)
{
    struct pkt_id p_id;
    pkt_read_id(&p_id, p->buf);

    strip64(p->name, p_id.username);

    if(conf.verifynames && !check_name(p->name, p_id.key))
    {
        llist_find_remove(&players, p);
        char reason[64];
        pad64(reason, "username verification failed");
        byte_type id = 0x0e;
        write(p->fd, &id, 1);
        write(p->fd, reason, 64);
        close(p->fd);
        FD_CLR(p->fd, &master_fds);
        free(p);
        return;
    }

    if(db_player_has_permission(p->name, PERMISSION_BANNED))
    {
        llist_find_remove(&players, p);
        char reason[64];
        pad64(reason, "you are banned");
        byte_type id = 0x0e;
        write(p->fd, &id, 1);
        write(p->fd, reason, 64);
        close(p->fd);
        FD_CLR(p->fd, &master_fds);
        free(p);
        return;
    }

    if(strchr(p->name, ' '))
    {
        llist_find_remove(&players, p);
        char reason[64];
        pad64(reason, "invalid username");
        byte_type id = 0x0e;
        write(p->fd, &id, 1);
        write(p->fd, reason, 64);
        close(p->fd);
        FD_CLR(p->fd, &master_fds);
        free(p);
        return;
    }

    if((p->id = player_get_id()) == -1)
    {
        FD_CLR(p->fd, &master_fds);
        player_disconnect(p, "No player ID available");
        return;
    }
    p->connected = 1;
    handshake(p);

    lvl_send_main(p);
    for(struct llist_item *i = players.first; i; i = i->next)
    {
        struct player *q = i->val;
        if(p != q && strcmp(p->name, q->name) == 0)
        {
            player_disconnect(q, "logged in again");
            continue;
        }
        player_spawn(q, p);
        if(p != q)
            player_spawn(p, q);
    }

    if(db_player_get_id(p->name) == -1)
        for(size_t i = 0; i < conf.defaultgroups.size; i++)
            db_player_add_group_id(p->name, *(int *) conf.defaultgroups.items[i]);

    broadcast_msgf("&e%s connected", p->name);
    if(conf.welcome)
        player_msg(p, conf.welcome);
}

void handle_pkt_block(struct player *p)
{
    struct pkt_block p_block;
    pkt_read_block(&p_block, p->buf);

    player_set_block(p, p_block.x, p_block.y, p_block.z,
                     p_block.mode, p_block.block);
}

void handle_pkt_pos(struct player *p)
{
    struct pkt_pos p_pos;
    pkt_read_pos(&p_pos, p->buf);
    player_move(p, p_pos.x, p_pos.y, p_pos.z,
                p_pos.yaw, p_pos.pitch);
}

void handle_pkt_msg(struct player *p)
{
    struct pkt_msg p_msg;
    pkt_read_msg(&p_msg, p->buf);

    char in[65];
    strip64(in, p_msg.message);

    // allocates memory for p->psize if empty, adds space enough for another message otherwise
    p->pmsg = realloc(p->pmsg, strlen(p->pmsg) + strlen(in) + 1);
    strcat(p->pmsg, in); // add what the player sent to persistent storage
    if(strchr("<>", in[strlen(in)-1])){ // if either append char is present
        p->pmsg[strlen(p->pmsg)-1] = '\0'; // remove it from persistent
        if(in[strlen(in)-1] == '>'){
            strcat(p->pmsg, " ");
        }
        player_msg(p, "&eMessage appended!");
        return;
    }
    char msg[strlen(p->pmsg)];
    strcpy(msg, p->pmsg);
    p->pmsg[0] = '\0';

    if(msg[0] == '/')
        cmd_handle(p, msg);
    else if(msg[0] == '@')
    {
        if(msg[1] == '\0')
            p->state_chat = STATE_CHAT_NORMAL;
        else
        {
            char *name = strtok(msg + 1, " ");
            if(!name)
                player_msg(p, "&eno player specified");
            else
            {
                struct player *q = NULL;
                if(player_find_one(p, &q, name) > 1)
                    return;
                if(!q)
                {
                    player_msg(p, "&eplayer not found");
                    return;
                }

                char *msgtext = strtok(NULL, "");
                if(msgtext)
                {
                    player_msgf(q, "&e[>pm]&f %s: %s", p->name, msgtext);
                    player_msgf(p, "&e[<pm]&f %s: %s", q->name, msgtext);
                }
                else
                {
                    player_msgf(p, "&enow whispering to %s", q->name);
                    p->state_chat = STATE_CHAT_PM;
                    p->pm_id = q->id;
                }
            }
        }
    }
    else if(p->state_chat == STATE_CHAT_NORMAL)
        broadcast_msgf("%s: %s", p->name, msg);
    else if(p->state_chat == STATE_CHAT_PM)
    {
        struct llist_item *i;
        for(i = players.first; i; i = i->next)
            if(((struct player *) i->val)->id == p->pm_id)
                break;
        if(i)
        {
            struct player *q = i->val;
            player_msgf(q, "&e[>pm]&f %s: %s", p->name, msg);
            player_msgf(p, "&e[<pm]&f %s: %s", q->name, msg);
        }
    }
}

void ping()
{
    byte_type id = 0x01;
    for(struct llist_item *i = players.first; i; i = i->next)
        write(((struct player *) i->val)->fd, &id, 1);
}
