/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <arpa/inet.h>

#include "packets.h"

void strip64(char *result, char *s)
{
    result[64] = '\0';
    int i;
    for(i = 64; s[i-1] == ' '; i--)
        result[i-1] = '\0';
    while(i-->0)
        result[i] = s[i];
}

void pad64(char *result, char *s)
{
    int i;
    for(i = 0; s[i] != '\0' && i < 64; i++)
        result[i] = s[i];
    while(i < 64)
        result[i++] = ' ';
}

int pkt_size(int id)
{
    switch(id)
    {
        case 0x00: return 131;
        case 0x05: return 9;
        case 0x08: return 10;
        case 0x0d: return 66;
    }
    return -1;
}

void pkt_read_id(struct pkt_id *p, byte_type *buf)
{
    p->id = buf[0];
    p->version = buf[1];
    memcpy(&p->username, &buf[2], 64);
    memcpy(&p->key, &buf[66], 64);
    p->unused = buf[130];
}

void pkt_read_block(struct pkt_block *p, byte_type *buf)
{
    p->id = buf[0];
    memcpy(&p->x, &buf[1], 2);
    memcpy(&p->y, &buf[3], 2);
    memcpy(&p->z, &buf[5], 2);
    p->x = ntohs(p->x);
    p->y = ntohs(p->y);
    p->z = ntohs(p->z);
    p->mode = buf[7];
    p->block = buf[8];
}

void pkt_read_pos(struct pkt_pos *p, byte_type *buf)
{
    p->id = buf[0];
    p->player = buf[1];
    memcpy(&p->x, &buf[2], 2);
    memcpy(&p->y, &buf[4], 2);
    memcpy(&p->z, &buf[6], 2);
    p->yaw = buf[8];
    p->pitch = buf[9];
}

void pkt_read_msg(struct pkt_msg *p, byte_type *buf)
{
    p->id = buf[0];
    p->unused = buf[1];
    memcpy(&p->message, &buf[2], 64);
}
