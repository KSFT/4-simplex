/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#ifndef CONFIG_H
#define CONFIG_H

#include "utils.h"

struct config
{
    int port;
    int saveinterval;
    int heartbeatinterval;
    char *name;
    char *motd;
    char *welcome;
    int maxplayers;
    int verifynames;
    char *mainlvl;
    struct alist groups;
    struct alist playerdata;
    struct alist defaultgroups;
};

struct player_datum
{
    char *player;
    struct alist groups;
};

extern struct config conf;

int config_load();

#endif
