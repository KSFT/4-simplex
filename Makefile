.RECIPEPREFIX := >

objects = main.o command.o config.o db.o heartbeat.o level.o nbt.o packets.o\
player.o utils.o

objects += pdjson.o

CFLAGS = -g -std=gnu99 -Wall -Wextra -Werror -Wpedantic -D_GNU_SOURCE

pdjson.o: CFLAGS += -Wno-implicit-fallthrough

4-simplex: $(objects)
>   $(CC) -lz -lssl -lcrypto -lsqlite3 -o 4-simplex $(objects)
main.o: packets.h player.h heartbeat.h level.h command.h utils.h config.h
player.o: packets.h level.h utils.h
level.o: player.h nbt.h
command.o: utils.h
config.o: pdjson.h

.PHONY: clean
clean:
>   rm -f 4-simplex $(objects)

.PHONY: db
db: playerdata.db
playerdata.db:
ifndef op
>   $(warning warning: no op specified)
endif
>   ./db-init.sh $(op)
