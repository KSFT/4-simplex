/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#ifndef COMMAND_H
#define COMMAND_H

#include "player.h"

void cmd_handle(struct player *p, char *cmd);
void cmd_cuboid_block(struct player *p,
                      short_type x, short_type y, short_type z,
                      byte_type block);
void cmd_cuboid_cancel(struct player *p);
void cmd_block_cancel(struct player *p);

#endif
