/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#ifndef PACKETS_H
#define PACKETS_H

#include <stdint.h>

typedef uint8_t byte_type;
typedef int8_t sbyte_type;
typedef int16_t short_type;

// client to server packets

struct pkt_id // player id
{
    byte_type id;
    byte_type version;
    char username[64];
    char key[64];
    byte_type unused;
};

struct pkt_block // set block
{
    byte_type id;
    short_type x;
    short_type y;
    short_type z;
    byte_type mode;
    byte_type block;
};

struct pkt_pos // position orientation
{
    byte_type id;
    byte_type player;
    short_type x;
    short_type y;
    short_type z;
    byte_type yaw;
    byte_type pitch;
};

struct pkt_msg // message
{
    byte_type id;
    byte_type unused;
    char message[64];
};

void strip64(char *result, char *s);
void pad64(char *result, char *s);

int pkt_size(int id);

void pkt_read_id(struct pkt_id *p, byte_type *buf);
void pkt_read_block(struct pkt_block *p, byte_type *buf);
void pkt_read_pos(struct pkt_pos *p, byte_type *buf);
void pkt_read_msg(struct pkt_msg *p, byte_type *buf);

#endif
