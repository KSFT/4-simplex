/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include "utils.h"

int alist_init(struct alist *l)
{
    if((l->items = malloc(2 * sizeof(void *))) == NULL)
        return -1;

    l->size = 0;
    l->allocated = 2;
    return 0;
}

void alist_einit(struct alist *l)
{
    if(alist_init(l) == -1)
    {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
}

int alist_add(struct alist *l, void *i)
{
    if(l->size == l->allocated)
    {
        void *tmp = realloc(l->items, 2 * l->allocated * sizeof(void *));
        if(tmp == NULL)
            return -1;
        l->items = tmp;
        l->allocated *= 2;
    }
    l->items[l->size++] = i;
    return 0;
}

void alist_eadd(struct alist *l, void *i)
{
    if(alist_add(l, i) == -1)
    {
        perror("realloc");
        exit(EXIT_FAILURE);
    }
}

char *alist_join(struct alist *l, const char *sep)
{
    size_t len = 0;
    size_t seplen = strlen(sep);
    for(size_t i = 0; i < l->size; i++)
        len += strlen(l->items[i]) + seplen;

    char *result = malloc(max(len - seplen + 1, 1));
    if(!result)
        return NULL;
    result[0] = '\0';
    if(len == 0)
        return result;

    for(size_t i = 0, j = 0;; i++)
    {
        strcpy(&result[j], l->items[i]);
        j += strlen(l->items[i]) + seplen;
        if(i == l->size - 1)
            break;
        strcpy(&result[j - seplen], sep);
    }
    return result;
}

char *alist_ejoin(struct alist *l, const char *sep)
{
    char *result = alist_join(l, sep);
    if(!result)
    {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    return result;
}

void llist_init(struct llist *l)
{
    l->first = l->last = NULL;
}

int llist_append(struct llist *l, void *i)
{
    struct llist_item *new = malloc(sizeof(struct llist_item));
    if(new == NULL)
        return -1;
    new->val = i;
    new->prev = l->last;
    new->next = NULL;
    if(l->last)
        l->last = l->last->next = new;
    else
        l->first = l->last = new;
    return 0;
}

void llist_remove(struct llist *l, struct llist_item *i)
{
    if(l->first == i)
    {
        if(l->last == i)
            l->first = l->last = NULL;
        else
        {
            l->first = i->next;
            l->first->prev = NULL;
        }
    }
    else if(l->last == i)
    {
        l->last = i->prev;
        l->last->next = NULL;
    }
    else
    {
        i->prev->next = i->next;
        i->next->prev = i->prev;
    }
    free(i);
}

void llist_find_remove(struct llist *l, void *i)
{
    for(struct llist_item *j = l->first; j; j = j->next)
        if(j->val == i)
        {
            llist_remove(l, j);
            return;
        }
}

void *emalloc(size_t n)
{
    void *buf = malloc(n);
    if(buf == NULL)
    {
        perror("malloc");
        exit(EXIT_FAILURE);
    }
    return buf;
}

int min(int a, int b)
{
    return a < b ? a : b;
}

int max(int a, int b)
{
    return a > b ? a : b;
}

int estrtoi(const char *s)
{
    char *endptr;
    long int n = strtol(s, &endptr, 10);
    if(*s == '\0' || *endptr != '\0'
       || n < 0 || n > INT_MAX)
    {
        return -1;
    }
    return (int) n;
}

char *estrdup(const char *s)
{
    char *r = strdup(s);
    if(!r)
    {
        perror("strdup");
        exit(EXIT_FAILURE);
    }
    return r;
}
