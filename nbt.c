/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include "nbt.h"

int nbt_read_short(FILE *f, int16_t *s)
{
    int c[2];
    c[0] = getc(f);
    c[1] = getc(f);
    if(c[0] == -1 || c[1] == -1)
        return -1;
    *s = ((uint16_t) (c[0] << 8) + (uint16_t) c[1]);
    return 0;
}

int nbt_read_int(FILE *f, int32_t *i)
{
    char c[4];
    if(!fread(c, 4, 1, f))
        return -1;
    *i = ((uint32_t) (c[0] << 24)
          + (uint32_t) (c[1] << 16)
          + (uint32_t) (c[2] << 8)
          + (uint32_t) c[3]);
    return 0;
}

int16_t nbt_read_str(FILE *f, char **s)
{
    int16_t len;
    if(nbt_read_short(f, &len) == -1)
        return -1;
    if(!(*s = malloc(len + 1)))
        return -1;
    if(!fread(*s, len, 1, f))
    {
        free(*s);
        return -1;
    }
    (*s)[len] = '\0';
    return len;
}

int32_t nbt_read_bytearray(FILE *f, char **a)
{
    int32_t size;
    if(nbt_read_int(f, &size) == -1)
        return -1;
    if(!(*a = malloc(size)))
        return -1;
    if(!fread(*a, size, 1, f))
    {
        free(*a);
        return -1;
    }
    return size;
}

static int skipbytes(FILE *f, size_t n)
{
    char *bytes = malloc(n);
    if(!bytes || fread(bytes, 1, n, f) != n)
        return -1;
    free(bytes);
    return 0;
}

int nbt_compound_ignore(FILE *f)
{
    int type;
    int16_t name_len;

    while(1)
    {
        if((type = getc(f)) == EOF)
            return -1;
        if(type == 0)
            return 0;

        if(nbt_read_short(f, &name_len) == -1)
            return -1;
        if(skipbytes(f, name_len) == -1)
            return -1;

        if(nbt_tag_ignore(f, type) == -1)
            return -1;
    }
}

int nbt_tag_ignore(FILE *f, int type)
{
    int32_t size = 0;
    int list_type;
    int err = 0;

    switch(type)
    {
        case 1:
            err = skipbytes(f, 1);
            break;
        case 2:
            err = skipbytes(f, 2);
            break;
        case 3:
        case 5:
            err = skipbytes(f, 4);
            break;
        case 4:
        case 6:
            err = skipbytes(f, 8);
            break;
        case 7:
            if(nbt_read_int(f, &size) == -1 || size < 0)
                return -1;
            err = skipbytes(f, size);
            break;
        case 8:
            if(nbt_read_short(f, (int16_t *) &size) == -1
               || size < 0)
                return -1;
            err = skipbytes(f, size);
            break;
        case 9:
            if((list_type = getc(f)) == EOF)
                return -1;
            if(nbt_read_int(f, &size) == -1)
                return -1;
            while(size-- > 0)
                nbt_tag_ignore(f, list_type);
            break;
        case 10:
            if(nbt_compound_ignore(f) == -1)
                return -1;
            break;
        case 11:
            if(nbt_read_int(f, &size) == -1 || size < 0)
                return -1;
            err = skipbytes(f, 4*size);
            break;
        case 12:
            if(nbt_read_int(f, &size) == -1 || size < 0)
                return -1;
            err = skipbytes(f, 8*size);
            break;
        default:
            return -1;
    }
    return err;
}

void nbt_write_short(int fd, int16_t s)
{
    int16_t s_ = htons(s);
    write(fd, &s_, 2);
}

void nbt_write_int(int fd, int32_t s)
{
    int32_t s_ = htonl(s);
    write(fd, &s_, 4);
}

void nbt_write_str(int fd, const char *s)
{
    size_t len = strlen(s);
    nbt_write_short(fd, len);
    write(fd, s, len);
}

void nbt_write_bytearray(int fd, char *a, int len)
{
    nbt_write_int(fd, len);
    write(fd, a, len);
}
