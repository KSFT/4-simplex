/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "defs.h"
#include "db.h"
#include "pdjson.h"
#include "utils.h"

#include "config.h"

struct config conf;

int config_load()
{
    conf.port = 25565;
    conf.saveinterval = 300;
    conf.heartbeatinterval = 45;
    conf.maxplayers = 8;
    conf.verifynames = 1;
    conf.name = NULL;
    conf.motd = NULL;
    conf.mainlvl = NULL;
    conf.welcome = NULL;

    if(alist_init(&conf.groups) == -1)
        return -1;

    FILE *f = fopen(FILE_CONFIG, "r");
    if(!f)
        return -1;

    json_stream j;
    json_open_stream(&j, f);
    json_next(&j);

    enum json_type t;
    const char *key;
    int err = -1;
    struct alist defaultgroups;
    if(alist_init(&defaultgroups) == -1)
        return -1;

    while(1)
    {
        t = json_next(&j);
        if(t == JSON_OBJECT_END)
        {
            err = 0;
            break;
        }
        if(t == JSON_ERROR)
            break;

        key = json_get_string(&j, NULL);
        if(strcmp(key, "port") == 0)
        {
            t = json_next(&j);
            if(t != JSON_NUMBER)
                break;
            if((conf.port = estrtoi(json_get_string(&j, NULL))) == -1)
                break;
        }
        else if(strcmp(key, "save-interval") == 0)
        {
            t = json_next(&j);
            if(t != JSON_NUMBER)
                break;
            if((conf.saveinterval = estrtoi(json_get_string(&j, NULL))) == -1)
                break;
        }
        else if(strcmp(key, "heartbeat-interval") == 0)
        {
            t = json_next(&j);
            if(t != JSON_NUMBER)
                break;
            if((conf.heartbeatinterval = estrtoi(json_get_string(&j, NULL)))
               == -1)
                break;
        }
        else if(strcmp(key, "max-players") == 0)
        {
            t = json_next(&j);
            if(t != JSON_NUMBER)
                break;
            if((conf.maxplayers = estrtoi(json_get_string(&j, NULL))) == -1)
                break;
        }
        else if(strcmp(key, "verify-names") == 0)
        {
            t = json_next(&j);
            if(t != JSON_NUMBER)
                break;
            if((conf.verifynames = estrtoi(json_get_string(&j, NULL))) == -1)
                break;
        }
        else if(strcmp(key, "name") == 0)
        {
            t = json_next(&j);
            if(t != JSON_STRING)
                break;
            conf.name = estrdup(json_get_string(&j, NULL));
        }
        else if(strcmp(key, "motd") == 0)
        {
            t = json_next(&j);
            if(t != JSON_STRING)
                break;
            conf.motd = estrdup(json_get_string(&j, NULL));
        }
        else if(strcmp(key, "main-level") == 0)
        {
            t = json_next(&j);
            if(t != JSON_STRING)
                break;
            conf.mainlvl = estrdup(json_get_string(&j, NULL));
        }
        else if(strcmp(key, "welcome") == 0)
        {
            t = json_next(&j);
            if(t != JSON_STRING)
                break;
            conf.welcome = estrdup(json_get_string(&j, NULL));
        }
        else if(strcmp(key, "default-groups") == 0)
        {
            t = json_next(&j);
            if(t != JSON_ARRAY)
                break;
            while((t = json_next(&j)) == JSON_STRING)
                if(alist_add(&defaultgroups, estrdup(json_get_string(&j, NULL))) == -1)
                    return -1;
            if(t != JSON_ARRAY_END)
                break;
        }
        else
            break;
    }

    json_close(&j);
    fclose(f);

    if(err == -1
       || conf.name == NULL
       || conf.motd == NULL
       || conf.mainlvl == NULL)
        return -1;
    if(alist_init(&conf.defaultgroups) == -1)
        return -1;

    int *groupid;
    for(size_t i = 0; i < defaultgroups.size; i++)
    {
        groupid = emalloc(sizeof(int));
        if((*groupid = db_group_get_id(defaultgroups.items[i])) == -1)
        {
            free(groupid);
            return -1;
        }
        else
        {
            if(alist_add(&conf.defaultgroups, groupid) == -1)
            {
                free(groupid);
                return -1;
            }
            free(defaultgroups.items[i]);
        }
    }
    free(defaultgroups.items);
    return 0;
}
