/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#ifndef LEVEL_H
#define LEVEL_H

#include "packets.h"
#include "player.h"

struct level
{
    char *fname;
    char *name;
    char *uuid;
    int changed;

    short_type x, y, z;
    byte_type *data;
    int32_t size;

    short_type spawn_x, spawn_y, spawn_z;
    byte_type spawn_yaw, spawn_pitch;
};

extern struct alist levels;
extern struct level main_lvl;

struct player;

void lvl_load_main();
void lvl_send_main(struct player *p);
void lvl_send(struct player *p, struct level *l);
void lvl_set_block(struct level *l, short_type x, short_type y, short_type z,
                   byte_type block);
int lvl_load(struct level *l, char *fname);
int lvl_save(struct level *l);
int lvl_gen_flat(struct level *l, char *fname, int x, int y, int z);

#endif
