/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#ifndef HEARTBEAT_H
#define HEARTBEAT_H

void heartbeat();

#endif
