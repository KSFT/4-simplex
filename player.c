/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version.
 */

#include <stdarg.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include "packets.h"
#include "command.h"

#include "player.h"

struct llist players;
struct llist ops;
FILE *ops_file;
byte_type assigned_ids[128];

void player_disconnect(struct player *p, char *reason)
{
    byte_type id = 0x0e;
    write(p->fd, &id, 1);

    char reason_padded[64];
    pad64(reason_padded, reason);
    write(p->fd, reason_padded, 64);

    llist_find_remove(&players, p);

    for(struct llist_item *i = players.first; i; i = i->next)
        player_despawn(i->val, p);

    assigned_ids[p->id] = 0;
    close(p->fd);
    FD_CLR(p->fd, &master_fds);
    FD_CLR(p->fd, &read_fds);
    broadcast_msgf("&e%s disconnected: %s", p->name, reason);
    free(p);
}

void player_move(struct player *p, short_type x, short_type y, short_type z,
                 byte_type yaw, byte_type pitch)
{
    p->x = x;
    p->y = y;
    p->z = z;
    p->yaw = yaw;
    p->pitch = pitch;

    for(struct llist_item *i = players.first; i; i = i->next)
    {
        struct player *q = i->val;
        if(q == p || !q->level || q->level != p->level)
            continue;
        byte_type id = 0x08;
        write(q->fd, &id, 1);
        write(q->fd, &p->id, 1);
        write(q->fd, &x, 2);
        write(q->fd, &y, 2);
        write(q->fd, &z, 2);
        write(q->fd, &yaw, 1);
        write(q->fd, &pitch, 1);
    }
}

void player_tp(struct player *p, short_type x, short_type y, short_type z,
               byte_type yaw, byte_type pitch)
{
    byte_type id = 0x08;
    byte_type pid = -1;
    write(p->fd, &id, 1);
    write(p->fd, &pid, 1);
    write(p->fd, &x, 2);
    write(p->fd, &y, 2);
    write(p->fd, &z, 2);
    write(p->fd, &yaw, 1);
    write(p->fd, &pitch, 1);
    player_move(p, x, y, z, yaw, pitch);
}

void player_msg_raw(struct player *p, const char *msg)
{
    byte_type id = 0x0d;
    write(p->fd, &id, 1);
    sbyte_type unused = 0x00;
    write(p->fd, &unused, 1);
    write(p->fd, msg, 64);
}

static void player_msg_color(struct player *p, const char *msg, char color, int continuing)
{
    char s[64];
    char currcolor = color;
    int i = 0, j = 0;
    if(continuing)
    {
        s[j++] = '>';
        s[j++] = ' ';
    }
    if(color != 'f')
    {
        s[j++] = '&';
        s[j++] = color;
    }

    while((msg[i] != '\0' && j < 64))
    {
        if((msg[i] == '%' || msg[i] == '&')
           && ((msg[i+1] >= '0' && msg[i+1] <= '9')
               || (msg[i+1] >= 'a' && msg[i+1] <= 'f')))
        {
            if(j == 63)
                break;

            if(msg[++i] != currcolor)
            {
                s[j++] = '&';
                currcolor = s[j++] = msg[i++];
            }
            continue;
        }

        if(msg[i] == '\n')
        {
            i++;
            break;
        }

        // splits the string at 64 characters
        // or at a space if the next space comes after 64 characters
        // or at a space if there are no more spaces and the whole string is
        // longer than 64 characters
        if(msg[i] == ' ')
        {
            char *next_space = strchr(msg + i + 1, ' ');
            if(j > 32 && (next_space - msg - i + j > 64
                          || (!next_space && strlen(msg) - i + j > 64)))
            {
                i++;
                break;
            }
        }


        s[j++] = msg[i++];
    }
    while(j < 64)
        s[j++] = ' ';
    if(msg[i] == '\0')
        player_msg_raw(p, s);
    else
    {
        player_msg_raw(p, s);
        player_msg_color(p, &msg[i], currcolor, 1);
    }
}

void player_msg(struct player *p, const char *msg)
{
    player_msg_color(p, msg, 'f', 0);
}

void player_msgf(struct player *p, char *fmt, ...)
{
    va_list ap;
    char *msg = NULL;
    va_start(ap, fmt);
    size_t len = vsnprintf(msg, 0, fmt, ap) + 1;
    va_end(ap);
    msg = emalloc(len);
    va_start(ap, fmt);
    vsprintf(msg, fmt, ap);
    va_end(ap);
    player_msg(p, msg);
    free(msg);
}

void broadcast_msg(const char *msg)
{
    for(struct llist_item *i = players.first; i; i = i->next)
        if(((struct player *) i->val)->connected)
            player_msg(i->val, msg);
    printf("%s\n", msg);
}

void broadcast_msgf(char *fmt, ...)
{
    va_list ap;
    char *msg = NULL;
    va_start(ap, fmt);
    int len = vsnprintf(msg, 0, fmt, ap) + 1;
    va_end(ap);
    msg = emalloc(len);

    va_start(ap, fmt);
    vsprintf(msg, fmt, ap);
    va_end(ap);

    broadcast_msg(msg);
    free(msg);
}

void player_init_ids()
{
    for(int i = 0; i < 128; i++)
        assigned_ids[i] = 0;
}

sbyte_type player_get_id()
{
    for(int i = 0; i < 128; i++)
        if(!assigned_ids[i])
        {
            assigned_ids[i] = 1;
            return i;
        }
    return -1;
}

static void undo_block(struct player *p, short_type x, short_type y, short_type z)
{
    struct level *l = p->level;

    short_type n_x, n_y, n_z;
    n_x = htons(x);
    n_y = htons(y);
    n_z = htons(z);

    byte_type id = 0x06;
    write(p->fd, &id, 1);
    write(p->fd, &n_x, 2);
    write(p->fd, &n_y, 2);
    write(p->fd, &n_z, 2);
    write(p->fd, &l->data[y*l->x*l->z+z*l->x+x+4], 1);
}

void player_set_block(struct player *p,
                      short_type x, short_type y, short_type z,
                      byte_type mode, byte_type block)
{
    if(p->state_block == STATE_BLOCK_NORMAL)
        lvl_set_block(p->level, x, y, z, mode ? block : 0x00);
    else if(p->state_block == STATE_BLOCK_CUBOID)
    {
        cmd_cuboid_block(p, x, y, z, block);
        undo_block(p, x, y, z);
    }
    else if(p->state_block == STATE_BLOCK_BLOCK)
        lvl_set_block(p->level, x, y, z, mode ? p->block_id : 0x00);
}

void player_despawn(struct player *p, struct player *a)
{
    byte_type id = 0x0c;
    write(p->fd, &id, 1);
    write(p->fd, &a->id, 1);
}

void player_spawn(struct player *p, struct player *a)
{
    sbyte_type self_id = -1;
    char name[64];
    pad64(name, a->name);

    byte_type id = 0x07;
    write(p->fd, &id, 1);
    if(a == p)
        write(p->fd, &self_id, 1);
    else
        write(p->fd, &a->id, 1);
    write(p->fd, name, 64);
    write(p->fd, &a->x, 2);
    write(p->fd, &a->y, 2);
    write(p->fd, &a->z, 2);
    write(p->fd, &a->yaw, 1);
    write(p->fd, &a->pitch, 1);
}

struct alist player_find(char *name)
{
    struct alist result;
    alist_einit(&result);

    for(struct llist_item *i = players.first; i; i = i->next)
    {
        struct player *p = i->val;
        if(strcmp(name, p->name) == 0)
        {
            alist_eadd(&result, p);
            return result;
        }
    }

    for(struct llist_item *i = players.first; i; i = i->next)
    {
        struct player *p = i->val;
        if(strcasecmp(name, p->name) == 0)
            alist_eadd(&result, p);
    }
    if(result.size)
        return result;

    for(struct llist_item *i = players.first; i; i = i->next)
    {
        struct player *p = i->val;
        if(strncasecmp(name, p->name, strlen(name)) == 0)
            alist_eadd(&result, p);
    }
    if(result.size)
        return result;

    for(struct llist_item *i = players.first; i; i = i->next)
    {
        struct player *p = i->val;
        if(strcasestr(p->name, name))
            alist_eadd(&result, p);
    }
    return result;
}

int player_find_one(struct player *p, struct player **q, char *name)
{
    struct alist l = player_find(name);
    if(l.size == 1)
    {
        *q = l.items[0];
        free(l.items);
        return 1;
    }
    else if(l.size == 0)
    {
        free(l.items);
        return 0;
    }

    char *list = alist_ejoin(&l, ", ");
    player_msgf(p, "&emultiple players match that name: %s", list);
    free(list);
    free(l.items);
    return 2;
}

int player_count()
{
    int count = 0;
    for(struct llist_item *i = players.first; i; i = i->next)
        count++;
    return count;
}

void player_block_switch(struct player *p)
{
    switch(p->state_block)
    {
        case STATE_BLOCK_CUBOID:
            cmd_cuboid_cancel(p);
            break;
        case STATE_BLOCK_BLOCK:
            cmd_block_cancel(p);
            break;
        case STATE_BLOCK_NORMAL:
            break;
    }
}
