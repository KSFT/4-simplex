/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#include <stdio.h>
#include <sqlite3.h>

#include "defs.h"
#include "db.h"

#define SQL_GROUP_ADD_PERMISSION                \
    "INSERT INTO Permission(pgroup, name)"      \
    " VALUES"                                   \
    " ("                                        \
    " ("                                        \
    " SELECT PGroup.id"                         \
    " FROM PGroup"                              \
    " WHERE PGroup.name = ?"                    \
    " ),"                                       \
    " ?"                                        \
    " );"                                       \

#define SQL_GROUP_CREATE                        \
    "INSERT INTO PGroup(name)"                  \
    " VALUES (?);"

#define SQL_GROUP_DELETE                        \
    "DELETE FROM PGroup"                        \
    " WHERE PGroup.name = ?;"

#define SQL_GROUP_GET_ID                        \
    "SELECT PGroup.id"                          \
    " FROM PGroup"                              \
    " WHERE PGroup.name = ?;"

#define SQL_GROUP_GET_LIST                      \
    "SELECT PGroup.name"                        \
    " FROM PGroup;"

#define SQL_GROUP_GET_PERMISSIONS               \
    "SELECT Permission.name"                    \
    " FROM Permission"                          \
    " INNER JOIN PGroup"                        \
    " ON Permission.pgroup = PGroup.id"         \
    " WHERE PGroup.name = ?;"

#define SQL_GROUP_GET_PLAYERS                   \
    "SELECT Player.name"                        \
    " FROM Player"                              \
    " INNER JOIN PlayerGroup"                   \
    " ON Player.id = PlayerGroup.player"        \
    " INNER JOIN PGroup"                        \
    " ON PlayerGroup.pgroup = PGroup.id"        \
    " WHERE PGroup.name = ?;"

#define SQL_GROUP_HAS_PERMISSION                \
    "SELECT *"                                  \
    " FROM PGroup"                              \
    " INNER JOIN Permission"                    \
    " ON PGroup.id = Permission.pgroup"         \
    " WHERE PGroup.name = ?"                    \
    " AND Permission.name = ?;"

#define SQL_GROUP_REMOVE_PERMISSION             \
    "DELETE FROM Permission"                    \
    " WHERE Permission.pgroup IN"               \
    " ("                                        \
    " SELECT PGroup.id"                         \
    " FROM PGroup"                              \
    " WHERE PGroup.name = ?"                    \
    " )"                                        \
    " AND Permission.name = ?;"

#define SQL_PLAYER_ADD                          \
    "INSERT INTO Player(name)"                  \
    " VALUES (?);"

#define SQL_PLAYER_ADD_GROUP                    \
    "INSERT INTO PlayerGroup(player, pgroup)"   \
    " SELECT Player.id, PGroup.id"              \
    " FROM Player, PGroup"                      \
    " WHERE Player.name = ?"                    \
    " AND PGroup.name = ?;"

#define SQL_PLAYER_ADD_GROUP_ID                 \
    "INSERT INTO PlayerGroup(player, pgroup)"   \
    " SELECT Player.id, PGroup.id"              \
    " FROM Player, PGroup"                      \
    " WHERE Player.name = ?"                    \
    " AND PGroup.id = ?;"

#define SQL_PLAYER_IS_GROUP                     \
    "SELECT PGroup.name"                        \
    " FROM PGroup"                              \
    " INNER JOIN PlayerGroup"                   \
    " ON PGroup.id = PlayerGroup.pgroup"        \
    " INNER JOIN Player"                        \
    " ON PlayerGroup.player = Player.id"        \
    " WHERE Player.name = ?"                    \
    " AND PGroup.name = ?;"

#define SQL_PLAYER_GET_GROUPS                   \
    "SELECT PGroup.name"                        \
    " FROM PGroup"                              \
    " INNER JOIN PlayerGroup"                   \
    " ON PGroup.id = PlayerGroup.pgroup"        \
    " INNER JOIN Player"                        \
    " ON PlayerGroup.player = Player.id"        \
    " WHERE Player.name = ?;"

#define SQL_PLAYER_GET_ID                       \
    "SELECT Player.id"                          \
    " FROM Player"                              \
    " WHERE name = ?;"

#define SQL_PLAYER_HAS_PERMISSION                               \
    "SELECT *"                                                  \
    " FROM Player"                                              \
    " INNER JOIN PlayerGroup"                                   \
    " ON Player.id = PlayerGroup.player"                        \
    " INNER JOIN Permission"                                    \
    " ON PlayerGroup.pgroup = Permission.pgroup"                \
    " WHERE Player.name = ?"                                    \
    " AND Permission.name = ?;"

#define SQL_PLAYER_REMOVE_GROUP                 \
    "DELETE FROM PlayerGroup"                   \
    " WHERE PlayerGroup.player IN"              \
    " ("                                        \
    " SELECT Player.id"                         \
    " FROM Player"                              \
    " WHERE Player.name = ?"                    \
    " )"                                        \
    " AND PlayerGroup.pgroup IN"                \
    " ("                                        \
    " SELECT PGroup.id"                         \
    " FROM PGroup"                              \
    " WHERE PGroup.name = ?"                    \
    " );"

static sqlite3 *db;
static sqlite3_stmt *group_add_permission,
    *group_create,
    *group_delete,
    *group_get_id,
    *group_get_list,
    *group_get_permissions,
    *group_get_players,
    *group_has_permission,
    *group_remove_permission,
    *player_add,
    *player_add_group,
    *player_add_group_id,
    *player_get_groups,
    *player_get_id,
    *player_has_permission,
    *player_is_group,
    *player_remove_group;

int db_init()
{
    group_add_permission =
        group_create =
        group_delete =
        group_get_id =
        group_get_list =
        group_get_permissions =
        group_get_players =
        group_has_permission =
        group_remove_permission =
        player_add =
        player_add_group =
        player_get_groups =
        player_get_id =
        player_has_permission =
        player_is_group =
        player_remove_group = NULL;

    if(sqlite3_open(FILE_PLAYERDATA, &db) != SQLITE_OK
       || (sqlite3_exec(db, "PRAGMA foreign_keys = ON;", NULL, NULL, NULL)
           != SQLITE_OK)
       || sqlite3_prepare_v2(db, SQL_GROUP_ADD_PERMISSION,
                             -1, &group_add_permission, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_GROUP_CREATE,
                             -1, &group_create, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_GROUP_DELETE,
                             -1, &group_delete, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_GROUP_GET_ID,
                             -1, &group_get_id, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_GROUP_GET_LIST,
                             -1, &group_get_list, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_GROUP_GET_PERMISSIONS,
                             -1, &group_get_permissions, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_GROUP_GET_PLAYERS,
                             -1, &group_get_players, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_GROUP_HAS_PERMISSION,
                             -1, &group_has_permission, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_GROUP_REMOVE_PERMISSION,
                             -1, &group_remove_permission, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_PLAYER_ADD,
                             -1, &player_add, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_PLAYER_ADD_GROUP,
                             -1, &player_add_group, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_PLAYER_ADD_GROUP_ID,
                             -1, &player_add_group_id, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_PLAYER_GET_GROUPS,
                             -1, &player_get_groups, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_PLAYER_GET_ID,
                             -1, &player_get_id, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_PLAYER_HAS_PERMISSION,
                             -1, &player_has_permission, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_PLAYER_IS_GROUP,
                             -1, &player_is_group, NULL) != SQLITE_OK
       || sqlite3_prepare_v2(db, SQL_PLAYER_REMOVE_GROUP,
                             -1, &player_remove_group, NULL) != SQLITE_OK)
    {
        sqlite3_finalize(group_add_permission);
        sqlite3_finalize(group_create);
        sqlite3_finalize(group_delete);
        sqlite3_finalize(group_get_id);
        sqlite3_finalize(group_get_list);
        sqlite3_finalize(group_get_permissions);
        sqlite3_finalize(group_get_players);
        sqlite3_finalize(group_has_permission);
        sqlite3_finalize(group_remove_permission);
        sqlite3_finalize(player_add);
        sqlite3_finalize(player_add_group);
        sqlite3_finalize(player_get_groups);
        sqlite3_finalize(player_get_id);
        sqlite3_finalize(player_has_permission);
        sqlite3_finalize(player_is_group);
        sqlite3_finalize(player_remove_group);
        sqlite3_close(db);
        return -1;
    }
    return 0;
}

void db_group_add_permission(const char *group, const char *permission)
{
    sqlite3_reset(group_add_permission);
    sqlite3_bind_text(group_add_permission, 1, group, -1, NULL);
    sqlite3_bind_text(group_add_permission, 2, permission, -1, NULL);
    sqlite3_step(group_add_permission);
}

void db_group_create(const char *group)
{
    sqlite3_reset(group_create);
    sqlite3_bind_text(group_create, 1, group, -1, NULL);
    sqlite3_step(group_create);
}

void db_group_delete(const char *group)
{
    sqlite3_reset(group_delete);
    sqlite3_bind_text(group_delete, 1, group, -1, NULL);
    sqlite3_step(group_delete);
}

int db_group_get_id(const char *group)
{
    sqlite3_reset(group_get_id);
    sqlite3_bind_text(group_get_id, 1, group, -1, NULL);
    if(sqlite3_step(group_get_id) == SQLITE_ROW)
        return sqlite3_column_int(group_get_id, 0);
    return -1;
}

struct alist *db_group_get_list()
{
    sqlite3_reset(group_get_list);

    struct alist *groups = emalloc(sizeof(struct alist));
    alist_einit(groups);
    while(sqlite3_step(group_get_list) == SQLITE_ROW)
    {
        char *group =
            estrdup((const char *)
                    sqlite3_column_text(group_get_list, 0));
        alist_eadd(groups, group);
    }
    return groups;
}

struct alist *db_group_get_permissions(const char *group)
{
    sqlite3_reset(group_get_permissions);
    sqlite3_bind_text(group_get_permissions, 1, group, -1, NULL);

    struct alist *permissions = emalloc(sizeof(struct alist));
    alist_einit(permissions);
    while(sqlite3_step(group_get_permissions) == SQLITE_ROW)
    {
        char *permission =
            estrdup((const char *)
                    sqlite3_column_text(group_get_permissions, 0));
        alist_eadd(permissions, permission);
    }
    return permissions;
}

struct alist *db_group_get_players(const char *group)
{
    sqlite3_reset(group_get_players);
    sqlite3_bind_text(group_get_players, 1, group, -1, NULL);

    struct alist *players = emalloc(sizeof(struct alist));
    alist_einit(players);
    while(sqlite3_step(group_get_players) == SQLITE_ROW)
    {
        char *player =
            estrdup((const char *) sqlite3_column_text(group_get_players, 0));
        alist_eadd(players, player);
    }
    return players;
}

int db_group_has_permission(const char *group, const char *permission)
{
    sqlite3_reset(group_has_permission);
    sqlite3_bind_text(group_has_permission, 1, group, -1, NULL);
    sqlite3_bind_text(group_has_permission, 2, permission, -1, NULL);
    if(sqlite3_step(group_has_permission) == SQLITE_ROW)
        return 1;
    return 0;
}

void db_group_remove_permission(const char *group, const char *permission)
{
    sqlite3_reset(group_remove_permission);
    sqlite3_bind_text(group_remove_permission, 1, group, -1, NULL);
    sqlite3_bind_text(group_remove_permission, 2, permission, -1, NULL);
    sqlite3_step(group_remove_permission);
}

void db_player_add(const char *player)
{
    sqlite3_reset(player_add);
    sqlite3_bind_text(player_add, 1, player, -1, NULL);
    sqlite3_step(player_add);
}

void db_player_add_group(const char *player, const char *group)
{
    if(db_player_get_id(player) == -1)
        db_player_add(player);
    sqlite3_reset(player_add_group);
    sqlite3_bind_text(player_add_group, 1, player, -1, NULL);
    sqlite3_bind_text(player_add_group, 2, group, -1, NULL);
    sqlite3_step(player_add_group);
}

void db_player_add_group_id(const char *player, int group_id)
{
    if(db_player_get_id(player) == -1)
        db_player_add(player);
    sqlite3_reset(player_add_group_id);
    sqlite3_bind_text(player_add_group_id, 1, player, -1, NULL);
    sqlite3_bind_int(player_add_group_id, 2, group_id);
    sqlite3_step(player_add_group_id);
}

struct alist *db_player_get_groups(const char *player)
{
    sqlite3_reset(player_get_groups);
    sqlite3_bind_text(player_get_groups, 1, player, -1, NULL);

    struct alist *groups = emalloc(sizeof(struct alist));
    alist_einit(groups);
    while(sqlite3_step(player_get_groups) == SQLITE_ROW)
    {
        char *player =
            estrdup((const char *) sqlite3_column_text(player_get_groups, 0));
        alist_eadd(groups, player);
    }
    return groups;
}

int db_player_get_id(const char *player)
{
    sqlite3_reset(player_get_id);
    sqlite3_bind_text(player_get_id, 1, player, -1, NULL);
    if(sqlite3_step(player_get_id) == SQLITE_ROW)
        return sqlite3_column_int(player_get_id, 0);
    return -1;
}

int db_player_has_permission(const char *player, const char *permission)
{
    sqlite3_reset(player_has_permission);
    sqlite3_bind_text(player_has_permission, 1, player, -1, NULL);
    sqlite3_bind_text(player_has_permission, 2, permission, -1, NULL);
    if(sqlite3_step(player_has_permission) == SQLITE_ROW)
        return 1;
    return 0;
}

int db_player_is_group(const char *player, const char *group)
{
    sqlite3_reset(player_is_group);
    sqlite3_bind_text(player_is_group, 1, player, -1, NULL);
    sqlite3_bind_text(player_is_group, 2, group, -1, NULL);
    if(sqlite3_step(player_is_group) == SQLITE_ROW)
        return 1;
    return 0;
}

void db_player_remove_group(const char *player, const char *group)
{
    sqlite3_reset(player_remove_group);
    sqlite3_bind_text(player_remove_group, 1, player, -1, NULL);
    sqlite3_bind_text(player_remove_group, 2, group, -1, NULL);
    sqlite3_step(player_remove_group);
}
