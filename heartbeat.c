/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include "defs.h"
#include "config.h"
#include "player.h"

void heartbeat()
{
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(80);
    struct hostent *he = gethostbyname("www.classicube.net");
    memcpy(&addr.sin_addr, (struct in_addr *) he->h_addr_list[0], he->h_length);
    if(connect(sock, (struct sockaddr *) &addr, sizeof(addr)) == -1)
        printf("error\n");
    char req[250];
    sprintf(req, "GET /server/heartbeat?port=%d&name=%s&public=True&version=7&salt=0&users=%d&max=%d&software=%s HTTP/1.1\r\nHost: www.classicube.net\r\n\r\n", conf.port, conf.name, player_count(), conf.maxplayers, NAME);
    write(sock, req, strlen(req));
    char c;
    read(sock, &c, 1);
    close(sock);
}
