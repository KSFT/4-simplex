/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

/*
 * Here are a few functions for parsing NBT files. The *_ignore functions don't
 * don't extract data but just read a tag other than type 0, starting
 * immediately after its type byte, and skip to the end of it.
 *
 * All of these functions return -1 on error, which might be a bad file or a
 * lack of memory. On success, nbt_read_str() and nbt_read_bytearray() return
 * the size of the tag they read, and the others return 0.
 *
 * There are also some functions for writing NBT files, which take POSIX file
 * descriptors instead of FILE pointers, because that happened to be more
 * convenient for 4-simplex.
 */

#ifndef NBT_H
#define NBT_H

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

int nbt_read_short(FILE *f, int16_t *s);
int nbt_read_int(FILE *f, int32_t *i);
int16_t nbt_read_str(FILE *f, char **s);
int32_t nbt_read_bytearray(FILE *f, char **a);
int nbt_tag_ignore(FILE *f, int type);
int nbt_compound_ignore(FILE *f);

void nbt_write_short(int fd, int16_t s);
void nbt_write_int(int fd, int32_t s);
void nbt_write_str(int fd, const char *s);
void nbt_write_bytearray(int fd, char *a, int len);

#endif
