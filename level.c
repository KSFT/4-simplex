/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/random.h>
#include <arpa/inet.h>
#include <zlib.h>
#include "utils.h"
#include "player.h"
#include "packets.h"
#include "nbt.h"

#include "level.h"

struct alist levels;
struct level main_lvl;

void lvl_send_main(struct player *p)
{
    lvl_send(p, &main_lvl);
}

void lvl_send(struct player *p, struct level *l)
{
    int size = 1.1 * l->size;
    byte_type *out_buf = emalloc(size);

    z_stream stream = {0};
    stream.next_in = l->data;
    stream.avail_in = l->size;
    stream.next_out = out_buf;
    stream.avail_out = size;

    if(deflateInit2(&stream, Z_DEFAULT_COMPRESSION, Z_DEFLATED, MAX_WBITS + 16,
                    MAX_MEM_LEVEL, Z_DEFAULT_STRATEGY) != Z_OK
       || deflate(&stream, Z_FINISH) != Z_STREAM_END
       || deflateEnd(&stream) != Z_OK)
    {
        fprintf(stderr, "zlib error\n");
        exit(EXIT_FAILURE);
    }

    size -= stream.avail_out;

    byte_type id = 0x02;
    write(p->fd, &id, 1);
    id = 0x03;
    short_type len = htons(1024);
    byte_type pct_complete = 50;
    byte_type *pos = out_buf;
    for(; size > 1024; size-=1024, pos+=1024)
    {
        write(p->fd, &id, 1);
        write(p->fd, &len, 2);
        write(p->fd, pos, 1024);
        write(p->fd, &pct_complete, 1);
    }
    len = htons((short_type) size);
    write(p->fd, &id, 1);
    write(p->fd, &len, 2);
    write(p->fd, pos, size);
    byte_type padding = 0x00;
    for(int i = size; i < 1024; i++)
        write(p->fd, &padding, 1);
    write(p->fd, &pct_complete, 1);

    id = 0x04;
    write(p->fd, &id, 1);

    l->x = htons(l->x);
    l->y = htons(l->y);
    l->z = htons(l->z);
    write(p->fd, &l->x, 2);
    write(p->fd, &l->y, 2);
    write(p->fd, &l->z, 2);
    l->x = ntohs(l->x);
    l->y = ntohs(l->y);
    l->z = ntohs(l->z);

    p->level = &main_lvl;

    p->x = htons(64<<5);
    p->y = htons(66<<5);
    p->z = htons(64<<5);
    p->pitch = 0;
    p->yaw = 0;
    free(out_buf);
}

void lvl_set_block(struct level *l, short_type x, short_type y, short_type z,
                   byte_type block)
{
    l->data[y*l->x*l->z+z*l->x+x+4] = block;

    short_type n_x, n_y, n_z;
    n_x = htons(x);
    n_y = htons(y);
    n_z = htons(z);
    for(struct llist_item *i = players.first; i; i = i->next)
    {
        struct player *p = i->val;
        if(p->level != l)
            continue;
        byte_type id = 0x06;
        write(p->fd, &id, 1);
        write(p->fd, &n_x, 2);
        write(p->fd, &n_y, 2);
        write(p->fd, &n_z, 2);
        write(p->fd, &block, 1);
    }
    l->changed = 1;
}

int lvl_gen_flat(struct level *l, char *fname, int x, int y, int z)
{
    l->fname = fname;
    if((l->uuid = malloc(16)) == NULL)
        return -1;
    getrandom(l->uuid, 16, 0);
    l->x = x;
    l->y = y;
    l->z = z;
    l->data = malloc(x*y*z + 4);

    int32_t bytes = htonl((int32_t) x*y*z);
    memcpy(l->data, &bytes, 4);

    for(int i = 0; i < x; i++)
        for(int k = 0; k < z; k++)
        {
            for(int j = 0; j < y/2 - 1; j++)
                l->data[j*x*z+k*x+i+4] = 3;
            l->data[(y/2)*x*z+k*x+i+4] = 2;
        }
    l->spawn_x = x/2;
    l->spawn_y = y/2 + 1;
    l->spawn_z = z/2;
    l->spawn_yaw = l->spawn_pitch = 0;
    l->size = x*y*z;
    l->changed = 1;
    return 0;
}

static int read_spawn(FILE *f, struct level *l)
{
    int type;
    char *name;
    int c;
    while(1)
    {
        if((type = getc(f)) == EOF)
            return -1;
        if(type == 0)
            return 0;
        if(nbt_read_str(f, &name) == -1)
            return -1;
        switch(type)
        {
            case 1:
                if(strcmp(name, "H") == 0)
                {
                    free(name);
                    if((c = getc(f)) == EOF)
                        return -1;
                    else
                        l->spawn_yaw = c;
                }
                else if(strcmp(name, "P") == 0)
                {
                    free(name);
                    if((c = getc(f)) == EOF)
                        return -1;
                    else
                        l->spawn_pitch = c;
                }
                else
                {
                    free(name);
                    return -1;
                }
                break;
            case 2:
                if(strcmp(name, "X") == 0)
                {
                    free(name);
                    if(nbt_read_short(f, &l->spawn_x) == -1)
                        return -1;
                }
                else if(strcmp(name, "Y") == 0)
                {
                    free(name);
                    if(nbt_read_short(f, &l->spawn_y) == -1)
                        return -1;
                }
                else if(strcmp(name, "Z") == 0)
                {
                    free(name);
                    if(nbt_read_short(f, &l->spawn_z) == -1)
                        return -1;
                }
                else
                {
                    free(name);
                    return -1;
                }
                break;
            default:
            {
                free(name);
                return -1;
            }
        }
    }
}

static int cw_load_tag(FILE *f, int type, char *name, struct level *l)
{
    char *payload;
    int32_t size;
    switch(type)
    {
        case 1:
            if(strcmp(name, "FormatVersion") == 0)
            {
                if(getc(f) != 1)
                    return -1;
            }
            else
                return -1;
            break;
        case 2:
            if(strcmp(name, "X") == 0)
            {
                if(nbt_read_short(f, &l->x) == -1)
                    return -1;
            }
            else if(strcmp(name, "Y") == 0)
            {
                if(nbt_read_short(f, &l->y) == -1)
                    return -1;
            }
            else if(strcmp(name, "Z") == 0)
            {
                if(nbt_read_short(f, &l->z) == -1)
                    return -1;
            }
            else
                return -1;
            break;
        case 7:
            if((size = nbt_read_bytearray(f, &payload)) == -1)
                return -1;
            if(strcmp(name, "UUID") == 0)
            {
                if(size != 16)
                {
                    free(payload);
                    return -1;
                }
                l->uuid = payload;
            }
            else if(strcmp(name, "BlockArray") == 0)
            {
                l->size = size;
                l->data = malloc(size + 4);
                int32_t besize = htonl(size);
                memcpy(l->data, &besize, 4);
                memcpy(l->data + 4, payload, size);
                free(payload);
            }
            else
            {
                free(payload);
                return -1;
            }
            break;
        case 8:
            nbt_read_str(f, &payload);
            if(strcmp(name, "Name") == 0)
                l->name = name;
            else
            {
                free(payload);
                return -1;
            }
            break;
        case 10:
            if(strcmp(name, "CreatedBy") == 0
               || strcmp(name, "MapGenerator") == 0
               || strcmp(name, "Metadata") == 0)
            {
                if(nbt_compound_ignore(f) == -1)
                    return -1;
            }
            else if(strcmp(name, "Spawn") == 0)
            {
                if(read_spawn(f, l) == -1)
                    return -1;
            }
            else
                return -1;
            break;
        default:
            return -1;
    }
    return 0;
}

static int cw_load(struct level *l, FILE *f)
{
    l->size
        = l->x = l->y = l->z
        = l->spawn_x = l->spawn_y = l->spawn_z = -1;
    l->spawn_yaw = l->spawn_pitch = 0;
    l->name = l->uuid = NULL;
    l->data = NULL;

    int type = getc(f);
    if(type != 10)
        return -1;

    char *name;
    if(nbt_read_str(f, &name) == -1 || strcmp(name, "ClassicWorld") != 0)
        return -1;
    free(name);

    while(1)
    {
        type = getc(f);
        if(type == EOF)
            return -1;
        if(type == 0)
            break;
        if(nbt_read_str(f, &name) == -1)
            return -1;
        if(cw_load_tag(f, type, name, l) == -1)
        {
            free(name);
            return -1;
        }
        free(name);
    }

    if(l->x == -1 || l->y == -1 || l->z == -1
       || l->size == -1 || l->size != l->x * l->y * l->z
       || l->spawn_x == -1 || l->spawn_y == -1 || l->spawn_z == -1
       || l->data == NULL || /*l->name == NULL ||*/ l->uuid == NULL)
        return -1;
    return 0;
}

int lvl_load(struct level *l, char *fname)
{
    int fds[2];
    if(pipe(fds) == -1)
    {
        perror("pipe");
        return -1;
    }
    int pid = fork();
    if(pid == -1)
    {
        perror("fork");
        return -1;
    }
    else if(pid == 0)
    {
        close(STDOUT_FILENO);
        close(fds[0]);
        dup2(fds[1], STDOUT_FILENO);
        execlp("gunzip", "gunzip", "-c", fname, NULL);
        exit(EXIT_FAILURE);
    }
    close(fds[1]);

    FILE *f = fdopen(fds[0], "r");
    if(!f)
        return -1;

    if(cw_load(l, f) == -1)
    {
        kill(pid, SIGKILL);
        wait(NULL);
        fclose(f);
        return -1;
    }
    wait(NULL);
    fclose(f);
    l->fname = fname;
    l->changed = 0;
    return 0;
}

int lvl_save(struct level *l)
{
    int fds[2];
    if(pipe(fds) == -1)
    {
        perror("pipe");
        return -1;
    }
    int pid = fork();
    if(pid == -1)
    {
        perror("fork");
        return -1;
    }
    else if(pid == 0)
    {
        close(fds[1]);
        dup2(fds[0], STDIN_FILENO);
        close(fds[0]);
        int fd = open(l->fname, O_WRONLY | O_CREAT, 0666);
        if(fd == -1 || dup2(fd, STDOUT_FILENO) == -1)
        {
            perror(l->fname);
            return -1;
        }
        close(fd);
        execlp("gzip", "gzip", NULL);
        exit(EXIT_FAILURE);
    }
    close(fds[0]);
    int fd = fds[1];

    char c = 10;
    write(fd, &c, 1);
    nbt_write_str(fd, "ClassicWorld");

    c = 1;
    write(fd, &c, 1);
    nbt_write_str(fd, "FormatVersion");
    write(fd, &c, 1);

/*    c = 8;
    write(fd, &c, 1);
    nbt_write_str(fd, "Name");
    nbt_write_str(fd, l->name);*/

    c = 7;
    write(fd, &c, 1);
    nbt_write_str(fd, "UUID");
    nbt_write_bytearray(fd, l->uuid, 16);

    c = 2;
    write(fd, &c, 1);
    nbt_write_str(fd, "X");
    nbt_write_short(fd, l->x);
    write(fd, &c, 1);
    nbt_write_str(fd, "Y");
    nbt_write_short(fd, l->y);
    write(fd, &c, 1);
    nbt_write_str(fd, "Z");
    nbt_write_short(fd, l->z);

    c = 10;
    write(fd, &c, 1);
    nbt_write_str(fd, "Spawn");
    c = 2;
    write(fd, &c, 1);
    nbt_write_str(fd, "X");
    nbt_write_short(fd, l->spawn_x);
    write(fd, &c, 1);
    nbt_write_str(fd, "Y");
    nbt_write_short(fd, l->spawn_y);
    write(fd, &c, 1);
    nbt_write_str(fd, "Z");
    nbt_write_short(fd, l->spawn_z);
    c = 1;
    write(fd, &c, 1);
    nbt_write_str(fd, "H");
    write(fd, &l->spawn_yaw, 1);
    write(fd, &c, 1);
    nbt_write_str(fd, "P");
    write(fd, &l->spawn_pitch, 1);
    c = 0;
    write(fd, &c, 1);

    c = 7;
    write(fd, &c, 1);
    nbt_write_str(fd, "BlockArray");
    nbt_write_bytearray(fd, (char *) l->data + 4, l->x * l->y * l->z);

    c = 10;
    write(fd, &c, 1);
    nbt_write_str(fd, "Metadata");
    c = 0;
    write(fd, &c, 1);

    c = 0;
    write(fd, &c, 1);
    close(fd);
    wait(NULL);
    l->changed = 0;
    return 0;
}
