/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#ifndef DEFS_H
#define DEFS_H

#define NAME "4-simplex"

#define FILE_CONFIG "config.json"
#define FILE_PLAYERDATA "playerdata.db"

#define PERMISSION_BANNED "banned"
#define PERMISSION_CUBOID "cuboid"
#define PERMISSION_GROUP_ADD "group-add"
#define PERMISSION_GROUP_ALL "group-all"
#define PERMISSION_GROUP_CREATE "group-create"
#define PERMISSION_GROUP_DELETE "group-delete"
#define PERMISSION_GROUP_REMOVE "group-remove"
#define PERMISSION_GROUP_SETALL "group-setall"
#define PERMISSION_KICK "kick"
#define PERMISSION_ROLL "roll"
#define PERMISSION_SAVE "save"
#define PERMISSION_SUMMON "summon"
#define PERMISSION_TP "tp"

#endif
