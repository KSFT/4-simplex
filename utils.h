/* Copyright 2018
 * This file is licensed under the GNU GPL version 3 or any later version. See
 * the LICENSE file.
 */

#ifndef LIST_H
#define LIST_H

#include <stdlib.h>

struct alist
{
    void **items;
    size_t size, allocated;
};

struct llist_item
{
    void *val;
    struct llist_item *next, *prev;
};

struct llist
{
    struct llist_item *first, *last;
};

int alist_init(struct alist *l);
void alist_einit(struct alist *l);
int alist_add(struct alist *l, void *i);
void alist_eadd(struct alist *l, void *i);
char *alist_join(struct alist *l, const char *sep);
char *alist_ejoin(struct alist *l, const char *sep);

void llist_init(struct llist *l);
int llist_append(struct llist *l, void *i);
void llist_remove(struct llist *l, struct llist_item *i);
void llist_find_remove(struct llist *l, void *i);

void *emalloc(size_t n);

int min(int a, int b);
int max(int a, int b);

int estrtoi(const char *s);
char *estrdup(const char *s);

#endif
